package co.com.reto1.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://demo.automationtesting.in/Register.html")
public class WebAutomationSiteDemo extends PageObject{

	public static final Target CAMPO_NOMBRE = Target.the("Nombre").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[1]/div[1]/input"));
	public static final Target CAMPO_APELLIDO =Target.the("Apellidos").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[1]/div[2]/input"));
	public static final Target AREA_DIRECCION = Target.the("El lugar donde se escriben la direcci�n a registrar").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[2]/div/textarea"));
	public static final Target CAMPO_CORREO_ELECTRONICO = Target.the("Correo Electronico").located(org.openqa.selenium.By.xpath("//*[@id=\'eid\']/input"));
	public static final Target CAMPO_TELEFONO= Target.the("Correo Electronico").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[4]/div/input"));
	public static final Target CAMPO_SELECCION_GENERO = Target.the("Genero a registrar").located(By.name("radiooptions"));
	public static final Target CAMPO_HOBBIES = Target.the("Hobbies a registrar").located(By.id("checkbox1"));
	public static final Target CAMPO_MULTISELECT_IDIOMA = Target.the("Campo seleccionar idioma").located(By.id("msdd"));
	public static final Target CAMPO_MULTISELECT_ESPANOL = Target.the("Campo seleccionar espa�ol").located(By.xpath("//*[@id='basicBootstrapForm']/div[7]/div/multi-select/div[2]/ul/li[35]/a"));
	public static final Target CAMPO_SELECT_HABILIDADES = Target.the("Seleccionar habilidad").located(By.xpath("//*[@id=\'Skills\']"));
	public static final Target CAMPO_SELECT_HABILIDAD = Target.the("Seleccionar Backup Management").located(By.xpath("//*[@id=\'Skills\']/option[9]"));
	public static final Target CAMPO_SELECT_PAIS= Target.the("Seleccionar Pais").located(By.id("countries"));
	public static final Target CAMPO_SELECT_PAISES= Target.the("Pais").located(By.id("countries"));
	public static final Target CAMPO_SELECT_COLOMBIA = Target.the("Colombia").located(By.xpath("//*[@id=\'countries\']/option[53]"));
	public static final Target CAMPO_DIA_CUMPLEANOS = Target.the("D�a de nacimiento").located(By.id("daybox"));
	public static final Target CAMPO_MES_CUMPLEANOS = Target.the("Mes de nacimiento").locatedBy("//*[@id=\'basicBootstrapForm\']/div[11]/div[2]/select");
	public static final Target CAMPO_ANO_CUMPLEANOS = Target.the("A�o de nacimiento").located(By.id("yearbox"));
	public static final Target CAMPO_CONTRASENA = Target.the("Contrase�a").located(By.id("firstpassword"));
	public static final Target CAMPO_CONFIRMAR_CONTRASENA = Target.the("Confirmar Contrase�a").located(By.id("secondpassword"));
	public static final Target BOTON_SUBMIT = Target.the("Bot�n enviar formulario").located(By.id("submitbtn"));
	public static final Target MENSAJE_REGISTRO = Target.the("Mensaje de validaci�n de registro").locatedBy("/html/body/section/div[1]/div/div[2]/h4[1]");
}
