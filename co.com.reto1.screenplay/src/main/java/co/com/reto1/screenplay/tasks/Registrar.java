package co.com.reto1.screenplay.tasks;

import java.util.List;

import co.com.reto1.screenplay.ui.WebAutomationSiteDemo;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class Registrar implements Task{

	private String nombre; 
	private String apellidos; 
	private String direccion; 
	private String correo;		
	private String telefono;
	private String habilidad;
	private String pais;
	private String ano;
	private String mes;
	private String dia;
	private String contrasena;
	

	public Registrar(String nombre, String apellidos, String direccion, String correo, String telefono,
			String habilidad, String pais, String ano, String mes, String dia , String contrasena) {
		super();
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.direccion = direccion;
		this.correo = correo;
		this.telefono = telefono;
		this.habilidad = habilidad;
		this.pais = pais;
		this.ano = ano;
		this.mes = mes;
		this.dia = dia;
		this.contrasena = contrasena;
	}



	@Override
	public <T extends Actor> void performAs(T actor) {
		System.out.println(ano);

		actor.attemptsTo(Enter.theValue(nombre).into(WebAutomationSiteDemo.CAMPO_NOMBRE),
		Enter.theValue(apellidos).into(WebAutomationSiteDemo.CAMPO_APELLIDO),
		Enter.theValue(direccion).into(WebAutomationSiteDemo.AREA_DIRECCION),
		Enter.theValue(correo).into(WebAutomationSiteDemo.CAMPO_CORREO_ELECTRONICO),
		Enter.theValue(telefono).into(WebAutomationSiteDemo.CAMPO_TELEFONO),
		Click.on(WebAutomationSiteDemo.CAMPO_SELECCION_GENERO),
		Click.on(WebAutomationSiteDemo.CAMPO_HOBBIES),
		Click.on(WebAutomationSiteDemo.CAMPO_MULTISELECT_IDIOMA),
		Click.on(WebAutomationSiteDemo.CAMPO_MULTISELECT_ESPANOL),
		Click.on(WebAutomationSiteDemo.CAMPO_TELEFONO),
		SelectFromOptions.byVisibleText(habilidad).from(WebAutomationSiteDemo.CAMPO_SELECT_HABILIDADES),
		SelectFromOptions.byVisibleText(pais).from(WebAutomationSiteDemo.CAMPO_SELECT_PAIS),
		Click.on(WebAutomationSiteDemo.CAMPO_SELECT_PAISES),
		Click.on(WebAutomationSiteDemo.CAMPO_SELECT_COLOMBIA),
		SelectFromOptions.byVisibleText(dia).from(WebAutomationSiteDemo.CAMPO_DIA_CUMPLEANOS),
		SelectFromOptions.byVisibleText(mes).from(WebAutomationSiteDemo.CAMPO_MES_CUMPLEANOS),
		SelectFromOptions.byVisibleText(ano).from(WebAutomationSiteDemo.CAMPO_ANO_CUMPLEANOS),
		Enter.theValue("Carlos123").into(WebAutomationSiteDemo.CAMPO_CONTRASENA),
		Enter.theValue("Carlos123").into(WebAutomationSiteDemo.CAMPO_CONFIRMAR_CONTRASENA),
		Click.on(WebAutomationSiteDemo.BOTON_SUBMIT));
		
		
	}



	public static Registrar ElUsurioEnLaPagina(String nombre, String apellidos, String direccion, String correo, String telefono,
			String habilidad, String pais, String ano, String mes, String dia, String contrasena) {			
		return Tasks.instrumented(Registrar.class, nombre, apellidos,  direccion, correo, telefono, habilidad, pais, ano, mes, dia, contrasena);
	}

	

}
