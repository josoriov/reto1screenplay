package co.com.reto1.screenplay.questions;



import co.com.reto1.screenplay.ui.WebAutomationSiteDemo;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class ElMensaje implements Question<String>{

	public static ElMensaje es() {
		return new ElMensaje();
	}

	@Override
	public String answeredBy(Actor actor) {
		return Text.of(WebAutomationSiteDemo.MENSAJE_REGISTRO).viewedBy(actor).asString();
	}

}
