package co.com.reto1.screenplay.tasks;

import co.com.reto1.screenplay.ui.WebAutomationSiteDemo;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class Ingresar implements Task{
	
	private WebAutomationSiteDemo webAutomationSiteDemo;	

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(webAutomationSiteDemo));
	}

	public static Ingresar LaPaginaAutomationDemo() {
		return Tasks.instrumented(Ingresar.class);
	}

}
