#Author: jucosori@bancolombia.com.co
#language:es

@tag1
Característica: Gestionar Cita Médica
Como paciente 
Quiero realizar la solicitud de una cita médica
A través del sistema de Administración de Hospitales


@tag2
Escenario: Realizar el Registro de un Doctor
Dado que Carlos necesita registrar un nuevo doctor
Cuando el realiza el registro del mismo en el aplicativo de Administración de Hospitales
Entonces el verifica que se presente en pantalla el mensaje Datos guardados correctamente

