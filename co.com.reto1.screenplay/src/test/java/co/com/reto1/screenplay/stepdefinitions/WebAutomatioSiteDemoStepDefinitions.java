package co.com.reto1.screenplay.stepdefinitions;

import static org.hamcrest.Matchers.equalTo;

import java.util.List;

import org.openqa.selenium.WebDriver;

import co.com.reto1.screenplay.questions.ElMensaje;
import co.com.reto1.screenplay.tasks.Ingresar;
import co.com.reto1.screenplay.tasks.Registrar;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class WebAutomatioSiteDemoStepDefinitions {	
	
	List<String> Rdatos;
	

	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor carlos = Actor.named("Juan Carlos");
	
	@Before
	public void configuracionInicial() {
		carlos.can(BrowseTheWeb.with(hisBrowser));
	}
	
	@Dado("^que Carlos quiere acceder a la Web Automation Demo Site$")
	public void queCarlosQuiereAccederALaWebAutomationDemoSite() throws Exception {
		carlos.wasAbleTo(Ingresar.LaPaginaAutomationDemo());
	 
	}


	@Cuando("^el realiza el registro en la página$")
	public void elRealizaElRegistroEnLaPágina(DataTable dtDatosRegistro) throws Exception {
		List<List<String>> lDatos =dtDatosRegistro.raw();
		String nombre;
		String apellidos;
		String direccion;
		String correo;
		String telefono;
		String Habilidad;
		String Pais;
		String ano;
		String mes;
		String dia;
		String contrasena;
		for (int i = 1; i < lDatos.size(); i++) {
			nombre = lDatos.get(i).get(0).trim();
			apellidos=lDatos.get(i).get(1).trim();
			direccion = lDatos.get(i).get(2).trim();
			correo = lDatos.get(i).get(3).trim();
			telefono = lDatos.get(i).get(4).trim();
			Habilidad= lDatos.get(i).get(8).trim();
			Pais= lDatos.get(i).get(9).trim();
			ano= lDatos.get(i).get(11).trim();
			System.out.println(ano);
			mes= lDatos.get(i).get(12).trim();
			dia= lDatos.get(i).get(13).trim();
			contrasena= lDatos.get(i).get(14).trim();
			carlos.attemptsTo(Registrar.ElUsurioEnLaPagina(nombre, apellidos, direccion, correo,telefono,Habilidad, Pais, ano, mes, dia, contrasena));
		}	
			
	}

	@Entonces("^el verifica que se carga la pantalla con texto Double Click on Edit Icon to EDIT the Table Row$")
	public void elVerificaQueSeCargaLaPantallaConTextoDoubleClickOnEditIconToEDITTheTableRow()  {
		carlos.should(seeThat(ElMensaje.es(), equalTo("- Double Click on Edit Icon to EDIT the Table Row.")));
		
	}	
	
}
